package views;

import weka.core.Instances;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by andreacorda on 13/12/16.
 */
public class DetailsForm extends JFrame{
    private JTable table1;
    private JPanel rootPanel;
    private JButton closeButton;

    public DetailsForm(Instances dataset) {
        initializeForm();
        setCloseButtonActionListener();
        populateTable(dataset);


        showForm();
    }

    private void populateTable(Instances data) {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Property");
        model.addColumn("Value");
        model.addRow(new String [] {"Dataset Size", String.valueOf(data.size())});
        for(int i = 0; i< data.numAttributes() ; i++){
            if(data.attribute(i).isNumeric())
                model.addRow(new String [] {"Variance " + data.attribute(i).name(), String.valueOf(data.variance(i))});
        }

        table1.setModel(model);
    }

    private void setCloseButtonActionListener() {
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DetailsForm.this.dispose();
            }
        });
    }

    private void initializeForm() {
        setContentPane(rootPanel);
        setTitle("DatasetEditor");
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void showForm() {
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
