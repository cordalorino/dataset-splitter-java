package views;

//import com.sun.codemodel.internal.JOp;

import constants.Strings;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils;
import worker.DatabaseSplitter;
import worker.DatasetTester;
import worker.DatasetsContainer;
import worker.DatasetsEnum;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;

/**
 * Created by andreacorda on 13/12/16.
 */
public class MainForm extends JFrame {
    private final String newline = "\n";
    private JButton loadButton;
    private JButton chooseButton;
    private JTextField textField1;
    private JPanel rootPanel;
    private JLabel labelStatus;
    private JButton datasetInfoButton;
    private JButton startSplittingButton;
    private JTextField percentageTextEdit;
    private JTextField repetitionsEditText;
    private JButton saveButton;
    private JButton testButtonMario;
    public JLabel statusIcon;
    private JTextField thresholdTextEdit;
    private Logger logger = Logger.getLogger("DatasetEditor");
    private Instances data;
    public static DatasetsContainer generatedDatasets = null;
    //public static Instances generatedComplementaryDataset = null;
    public static String generatedDatasetFilename = "default";
    public static double generatedDatasetDelta = 0.0;


    public MainForm() {
        PropertyConfigurator.configure("log4j.properties");
        initializeForm();

        setFileChooserButtonAction();
        setLoadFileButtonAction();
        setDatasetInfoButtonAction();
        setSplittingButtonAction();
        setSaveButtonAction();
        setTestButtonAction();
        showForm();
    }

    public static void changeStatusIcon(boolean ready, JLabel statusIcon){

        String iconPath = ready ? "/icons/greeen_status.png" : "/icons/red_status.png";

        URL iconUrl = MainForm.class.getResource(iconPath);

        statusIcon.setIcon(new ImageIcon(iconUrl));

    }

    private void setSaveButtonAction() {
        saveButton.addActionListener(e ->
                {
                    String fileName = "";
                    if(generatedDatasets == null){
                        JOptionPane.showMessageDialog(MainForm.this, "Datasets not yet generated", "Error", JOptionPane.ERROR_MESSAGE);
                        return;
                    }

                    Instances reduced = generatedDatasets.getDatasetFromEnum(DatasetsEnum.REDUCED);
                    Instances comp = generatedDatasets.getDatasetFromEnum(DatasetsEnum.ORIGINAL);


                    if (reduced != null) {
                        //String fileName = JOptionPane.showInputDialog(MainForm.this,"Please insert file name","Insert File Name",JOptionPane.OK_CANCEL_OPTION);
                        fileName = Paths.get(getChosenArffFilePath()).getFileName().toString();

                        fileName = fileName.substring(0, fileName.lastIndexOf("."));

                        if (fileName != null) {
                            if (generatedDatasets.getDatasetFromEnum(DatasetsEnum.REDUCED) != null) {
                                try {
                                    saveDatasetToFile(fileName + " " + generatedDatasetFilename,reduced);
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            } else {
                                JOptionPane.showMessageDialog(MainForm.this, "Dataset not yet generated", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                    if (generatedDatasets.getDatasetFromEnum(DatasetsEnum.ORIGINAL) != null) {
                        //String fileName = JOptionPane.showInputDialog(MainForm.this,"Please insert file name","Insert File Name",JOptionPane.OK_CANCEL_OPTION);



                        if (fileName != null) {
                            if (generatedDatasets.getDatasetFromEnum(DatasetsEnum.ORIGINAL) != null) {
                                try {
                                    saveDatasetToFile(fileName + " -C " + generatedDatasetFilename, comp);
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            } else {
                                JOptionPane.showMessageDialog(MainForm.this, "Dataset not yet generated", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                }
        );
    }


    private void setTestButtonAction() {
        testButtonMario.addActionListener(e ->
                {
                    if (generatedDatasets != null) {

                        int percentage = Integer.parseInt(percentageTextEdit.getText());

                        String fileName = Paths.get(getChosenArffFilePath()).getFileName().toString();
                        fileName = fileName.substring(0, fileName.lastIndexOf("."));

                        DatasetTester datasetTester = new DatasetTester(labelStatus, statusIcon, percentage, data,
                                generatedDatasets, generatedDatasetDelta, fileName);

                        changeStatusIcon(false, statusIcon);

                        datasetTester.execute();
                    }
                }
        );
    }



    private void saveDatasetToFile(String name , Instances dataset) throws IOException {
        ArffSaver saver = new ArffSaver();
        saver.setInstances(dataset);
        File dir = new File("data");
        if (!dir.exists())
            dir.mkdir();
        saver.setFile(new File("./data/" + name + ".arff"));
        saver.writeBatch();

        labelStatus.setText("Saved reduced dataset to file");
    }

    private void setSplittingButtonAction() {
        startSplittingButton.addActionListener(e ->
                {
                    int percentage = Integer.parseInt(percentageTextEdit.getText());
                    int repetitions = Integer.parseInt(repetitionsEditText.getText());
                    double splitterThreshold = Double.parseDouble(thresholdTextEdit.getText());
                    DatabaseSplitter databaseSplitter = new DatabaseSplitter(labelStatus, statusIcon, data,
                            percentage, repetitions,splitterThreshold);

                    changeStatusIcon(false, statusIcon);

                    databaseSplitter.execute();
                }
        );
    }

    private void showForm() {
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initializeForm() {
        setContentPane(rootPanel);
        setTitle("DatasetEditor");
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        publishStatus("");
    }

    private void setDatasetInfoButtonAction() {
        datasetInfoButton.addActionListener(e ->
                {
                    new DetailsForm(data);
                }
        );
    }

    private void setLoadFileButtonAction() {
        loadButton.addActionListener(e ->
                {
                    if (shouldConsiderFilePath()) {
                        logger.info(Strings.STARTING_LOADING_FILE);
                        publishStatus(Strings.STARTING_LOADING_FILE);
                        Thread t = new Thread(() -> {
                            try {
                                ConverterUtils.DataSource source = new ConverterUtils.DataSource(getChosenArffFilePath());
                                data = source.getDataSet();
                                logger.info(Strings.FINISHED_LOADING_FILE);
                                logDatasetDetails();
                                publishStatus(Strings.FINISHED_LOADING_FILE);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }

                            changeStatusIcon(true, statusIcon);
                        });

                        changeStatusIcon(false , statusIcon);

                        t.start();
                    }
                }
        );
    }

    private void logDatasetDetails() {
        logger.debug("Selected Dataset: " + data.relationName());
        logger.debug("Instances Number: " + data.size());
    }

    private void publishStatus(String startingLoadingFile) {
        labelStatus.setText(startingLoadingFile);
    }

    private String getChosenArffFilePath() {
        return textField1.getText();
    }

    private boolean shouldConsiderFilePath() {
        return getChosenArffFilePath() != null && getChosenArffFilePath().length() > 0;
    }

    private void setFileChooserButtonAction() {
        chooseButton.addActionListener(e ->
                {
                    final JFileChooser fc = new JFileChooser();
                    FileFilter filter = new FileNameExtensionFilter("ARFF File", "arff");
                    fc.setFileFilter(filter);
                    int returnVal = fc.showOpenDialog(this);
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();
                        textField1.setText(file.getAbsolutePath());
                        //This is where a real application would open the file.
                        logger.info("Opening: " + file.getName() + "." + newline);

                    } else {
                        logger.info("Open command cancelled by user." + newline);
                    }
                }
        );
    }

}
