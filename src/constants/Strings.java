package constants;

/**
 * Created by andreacorda on 13/12/16.
 */
public class Strings {
    public static String STARTING_LOADING_FILE = "Starting charging the dataset";
    public static String FINISHED_LOADING_FILE = "Dataset charged";
}
