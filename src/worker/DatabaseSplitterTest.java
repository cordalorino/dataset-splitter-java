package worker;

import org.junit.Before;
import org.junit.Test;
import weka.core.AttributeStats;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

/**
 * Created by andreacorda on 15/12/16.
 */
public class DatabaseSplitterTest {


    private Instances dataSet;

    @Before
    public void initialize() throws Exception {
        ConverterUtils.DataSource source = new ConverterUtils.DataSource("/Users/andreacorda/Desktop/Weka/covtype.arff");
        dataSet = source.getDataSet();

    }

    @Test
    public void generateReducedDatasetUpdated() throws Exception {
        //DatabaseSplitter s = new DatabaseSplitter(null,dataSet,15,10);
        dataSet.setClassIndex(dataSet.numAttributes()-1);
        AttributeStats stats = dataSet.attributeStats(dataSet.classIndex());

        int [] instancesCountPerClass = stats.nominalCounts.clone();
        for(int i = 0; i< instancesCountPerClass.length; i++){
            instancesCountPerClass[i] = (int) (instancesCountPerClass[i] * 15 /100);
            System.out.println("Instances for class " + (i+1) + ": " + instancesCountPerClass[i]);
        }

        Instances reducedDataset = new Instances(dataSet);
        reducedDataset.delete();
       // s.generateReducedDatasetUpdated(instancesCountPerClass,reducedDataset);
    }

    @Test
    public void test(){

    }
}