package worker;

import weka.core.Instance;
import weka.core.Instances;

import java.util.Random;

/**
 * Created by andre on 19/12/2016.
 */



public class DatasetsContainer {

     Instances dataset;
     Instances reducedDataset;

    public DatasetsContainer(Instances dataset) {
        this.dataset = dataset;
        this.reducedDataset = new Instances(dataset, 0);
    }


    public Instances getDatasetFromEnum(DatasetsEnum d) {
        return d == worker.DatasetsEnum.ORIGINAL ? this.dataset : this.reducedDataset;
    }

    public void moveInstance(DatasetsEnum from, DatasetsEnum to, int index) {

        Instances fromDataset = getDatasetFromEnum(from);

        Instances toDataset = getDatasetFromEnum(to);

        Instance instance = fromDataset.get(index);

        toDataset.add(instance);

        fromDataset.remove(index);
    }


    public int getRandomIndexFromDataset(DatasetsEnum d) {

        Instances instances = getDatasetFromEnum(d);
        Random rand = new Random();

        return rand.nextInt(instances.size());
    }

    public int getRandomIndexFromDataset(DatasetsEnum d, double expectedClass) {

        Random rand = new Random();
        int i;
        Instances instances = getDatasetFromEnum(d);
        Instance extractedInstance = null;

        do {
            i = rand.nextInt(instances.size());
            extractedInstance = instances.get(i);
        } while (extractedInstance.classValue() != expectedClass);

        return i;
    }

    public Instance getInstance(DatasetsEnum d, int index) {
        return getDatasetFromEnum(d).get(index);
    }

    public void generateReducedDataset(int[] instancesCountPerClass) {

        while (countRemainingInstancesToConsider(instancesCountPerClass) > 0) {

            int index = getRandomIndexFromDataset(worker.DatasetsEnum.ORIGINAL);

            Instance instance = this.dataset.get(index);

            if (instancesCountPerClass[(int) instance.classValue()] > 0) {

                moveInstance(worker.DatasetsEnum.ORIGINAL, worker.DatasetsEnum.REDUCED, index);

                instancesCountPerClass[(int) instance.classValue()]--;
            }
        }
    }


    private int countRemainingInstancesToConsider(int[] instancesCountPerClass) {
        int res = 0;
        for (int instancesCountPerClas : instancesCountPerClass) {
            res += instancesCountPerClas;
        }
        return res;
    }


}