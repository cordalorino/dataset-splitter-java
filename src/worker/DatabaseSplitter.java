package worker;

//import com.sun.tools.javac.util.Pair;

import org.apache.log4j.Logger;
import utils.Utils;
import views.MainForm;
import weka.core.AttributeStats;
import weka.core.Instance;
import weka.core.Instances;

import javax.swing.*;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by andreacorda on 13/12/16.
 */
public class DatabaseSplitter extends SwingWorker<Void, String> {

    public static final double RANKER_THRESHOLD = -1.7976931348623157E308;

    public static double splitterThreshold;
    //public static final int SPLITTER_REPETITIONS = 50;


    JLabel statusLabel;
    private JLabel statusIcon;
    Instances dataSet;
    private double percentage;
    private int splitterRepetitions;
    private Path datasetPath;
    Logger logger = Logger.getLogger(DatabaseSplitter.class);
    private HashMap<Integer, Boolean> extractedHashMap = new HashMap<>();
    private ArrayList<Integer> extractedHashMapIntInt = new ArrayList<>();
    private ArrayList<Integer> availableIndexes = new ArrayList<>();

    public DatabaseSplitter(JLabel statusLabel, JLabel statusIcon, Instances dataSet, double percentage, int splitterRepetitions, double splitterThreshold) {
        this.statusLabel = statusLabel;
        this.statusIcon = statusIcon;
        this.dataSet = dataSet;
        this.percentage = percentage;

        this.splitterRepetitions = splitterRepetitions;

        this.splitterThreshold = splitterThreshold;
    }

    private void initializeAvailableIndexes(Instances dataSet) {
        int datasetSize = dataSet.size();
        for (int i = 0; i < datasetSize; i++) {
            availableIndexes.add(i, i);
        }
    }

    @Override
    protected Void doInBackground() throws Exception {


        logger.debug("Entered in the Splitter Thread");

        logAndPublishToFrame("Starting calculating InfoGain stats");
        double[][] infoGainOriginalDatabase = Utils.calculateInfoGain(dataSet, RANKER_THRESHOLD);
        logAndPublishToFrame("Finished calculating InfoGain stats");

        logAndPublishToFrame("Starting calculating DistributionCounter stats");
        //DistributionCounter[] distributionTable = createDistributionTableForDataset(dataSet);
        //distributionTable = reduceDistributionTable(percentage, distributionTable);

        DistributionCounter bestDistribution = new DistributionCounter(dataSet);
        bestDistribution = bestDistribution.reduceDistributionTable(percentage);

        logAndPublishToFrame("Finished calculating DistributionCounter stats");


        publish("Starting generating new dataset");
        dataSet.setClassIndex(dataSet.numAttributes() - 1);
        int[] instancesCountBackup = getReducedCountPerClass();

        double weightedDelta;
        DistributionCounter reducedDistribution;

        double deltas[];

        DatasetsContainer datasetsContainer = new DatasetsContainer(dataSet);

        int[] instancesCountPerClass = Arrays.copyOf(instancesCountBackup, instancesCountBackup.length);

        datasetsContainer.generateReducedDataset(instancesCountPerClass);

        logAndPublishToFrame("Finished adding instances");

        logAndPublishToFrame("Starting calculating stats");
        reducedDistribution = new DistributionCounter(datasetsContainer.reducedDataset);
        logAndPublishToFrame("Finished calculating stats");

        deltas = DistributionCounter.distributionDifference(bestDistribution,
                reducedDistribution, datasetsContainer.reducedDataset);

        weightedDelta = DistributionCounter.weightedDelta(deltas, infoGainOriginalDatabase);

        logger.debug("Weighted Delta: " + weightedDelta);

        int count = 0;

        while (weightedDelta >= splitterThreshold) {

            double newDeltaRemove = 0, newDeltaAdd;
            double[] newDeltasRemove, newDeltasAdd;
            double choosenClass = 0;

            int indexToRemove = 0;
            int indexToAdd = 0;
            Instance instanceToRemove = null;
            Instance instanceToAdd = null;

            count = 0;

            do {

                indexToRemove = datasetsContainer.getRandomIndexFromDataset(DatasetsEnum.REDUCED);
                instanceToRemove = datasetsContainer.getInstance(DatasetsEnum.REDUCED, indexToRemove);

               /* newDelta = DistributionCounter.weightedDeltaWithInstanceModification(
                        bestDistribution,
                        reducedDistribution,
                        infoGainOriginalDatabase,
                        deltas,
                        instanceToRemove,
                        datasetsContainer.reducedDataset,
                        false);*/


               newDeltasRemove = DistributionCounter.weightedDeltaWithInstanceModification(bestDistribution,
                       reducedDistribution,
                       infoGainOriginalDatabase,
                       deltas,
                       instanceToRemove,
                       datasetsContainer.reducedDataset,
                       false);

               newDeltaRemove = DistributionCounter.weightedDelta(newDeltasRemove, infoGainOriginalDatabase);


                if (newDeltaRemove < weightedDelta) {
                    choosenClass = instanceToRemove.classValue();
                }
                count++;

                //System.out.println("Remove loop count:" + count);

            } while (Math.abs(newDeltaRemove) >= Math.abs(weightedDelta) && count < splitterRepetitions);


            if (count >= splitterRepetitions || instanceToRemove == null) {
                logAndPublishToFrame("Can't find dataset under threshold");
                break;
            }


           count = 0;

            do {

                indexToAdd = datasetsContainer.getRandomIndexFromDataset(DatasetsEnum.ORIGINAL, choosenClass);

                instanceToAdd = datasetsContainer.getInstance(DatasetsEnum.ORIGINAL, indexToAdd);
/*
                newDelta = DistributionCounter.weightedDeltaWithInstanceModification(
                        bestDistribution,
                        reducedDistribution,
                        infoGainOriginalDatabase,
                        deltas,
                        instanceToAdd,
                        datasetsContainer.reducedDataset,
                        true);*/

                newDeltasAdd = DistributionCounter.weightedDeltaWithInstanceModification(bestDistribution,
                        reducedDistribution,
                        infoGainOriginalDatabase,
                        newDeltasRemove,
                        instanceToRemove,
                        datasetsContainer.reducedDataset,
                        false);

                newDeltaAdd = DistributionCounter.weightedDelta(newDeltasAdd, infoGainOriginalDatabase);

                count++;

                //System.out.println("Add loop count:" + count);

            } while (Math.abs(newDeltaAdd) >= Math.abs(weightedDelta) && count < splitterRepetitions);

            if (count >= splitterRepetitions || instanceToAdd == null) {
                logAndPublishToFrame("Can't find dataset under threshold");
                break;
            }

            if (Math.abs(newDeltaAdd) < Math.abs(weightedDelta)) {

                datasetsContainer.moveInstance(DatasetsEnum.ORIGINAL, DatasetsEnum.REDUCED, indexToAdd);
                datasetsContainer.moveInstance(DatasetsEnum.REDUCED, DatasetsEnum.ORIGINAL, indexToRemove);

                weightedDelta = newDeltaAdd;

                logger.debug("New Weighted Delta: " + weightedDelta);
            }



        }

        logger.debug("Weighted Delta: " + weightedDelta);

        MainForm.generatedDatasets = datasetsContainer;

        MainForm.generatedDatasetFilename = MessageFormat.format("-P {0} -D {1}",
                percentage, (new Double(weightedDelta)).toString());

        MainForm.generatedDatasetDelta = weightedDelta;

        logAndPublishToFrame("Finished calculating InfoGain stats");

        logAndPublishToFrame("Finished generating new dataset");

        MainForm.changeStatusIcon(true, statusIcon);

        logAndPublishToFrame("Creating test dataset");

        // generateComplementaryDataset();
        logger.info("Total instances in reduced " + datasetsContainer.reducedDataset.size());
        //logger.info("Difference " + (dataSet.size() - datasetsContainer.reducedDataset.size()));
        logAndPublishToFrame("Finished generatingtest dataset");


        return null;
    }


    private int[] getReducedCountPerClass() {
        AttributeStats stats = dataSet.attributeStats(dataSet.classIndex());

        int numberOfInstances = (int) Math.round(dataSet.size() * percentage / 100);
        int[] instancesCountBackup = Arrays.copyOf(stats.nominalCounts, stats.nominalCounts.length);
        Map<Integer, Double> m = new HashMap<>();

        for (int i = 0; i < instancesCountBackup.length; i++) {
            m.put(i, (instancesCountBackup[i] * percentage / 100));
            logger.info("Instances for class " + (i + 1) + ": " + instancesCountBackup[i]);
        }

        m = m.entrySet().stream().sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));

        for (int k : m.keySet()) {
            double value = m.get(k);
            if (numberOfInstances > 0) {
                instancesCountBackup[k] = (int) Math.ceil(value);
            } else {
                instancesCountBackup[k] = (int) Math.floor(value);
            }
            numberOfInstances -= instancesCountBackup[k];
        }

        return instancesCountBackup;
    }

/*
    private boolean isSameOrder(double[][] attrRankOriginalDatabase, double[][] attrRankNewDatabase) {
        boolean sameOrder = true;
        for (int i = 0; i < splitterRepetitions; i++) {
            if (attrRankOriginalDatabase[i][0] != attrRankNewDatabase[i][0]) {
                sameOrder = false;
            }
        }
        return sameOrder;
    }*/

    private void logAndPublishToFrame(String message) {
        logger.info(message);
        publish(message);
    }


    @Override
    protected void process(List<String> updates) {
        statusLabel.setText(updates.get(updates.size() - 1));
    }

    @Override
    protected void done() {
        try {

        } catch (Exception ignore) {
        }
    }


}
