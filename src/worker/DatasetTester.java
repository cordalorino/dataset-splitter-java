package worker;

import org.apache.log4j.Logger;
import views.MainForm;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.trees.J48;
import weka.core.Instances;

import javax.swing.*;
import java.io.File;
import java.text.MessageFormat;
import java.util.List;
import java.util.Random;

/**
 * Created by andre on 18/12/2016.
 */
public class DatasetTester extends SwingWorker<Void, String> {

    Logger logger = Logger.getLogger(DatasetTester.class);
    private JLabel statusLabel;
    private JLabel statusIcon;
    private double percentage;
    private Instances dataset;
    private DatasetsContainer datasetsContainer;
    private double delta;
    private String datasetName;

    public DatasetTester(JLabel statusLabel, JLabel statusIcon, double percentage, Instances dataset, DatasetsContainer datasetsContainer, double delta, String datasetName) {
        this.statusLabel = statusLabel;
        this.statusIcon = statusIcon;
        this.percentage = percentage;
        this.dataset = dataset;
        this.datasetsContainer = datasetsContainer;
        this.delta = delta;
        this.datasetName = datasetName;
    }

    @Override
    protected Void doInBackground() throws Exception {


        Classifier[] classifiers = {
                buildJ48(),
                buildNaiveBayes(),
                buildMultilayerPerceptron()};

        Instances train = datasetsContainer.getDatasetFromEnum(DatasetsEnum.REDUCED);
        Instances test = datasetsContainer.getDatasetFromEnum(DatasetsEnum.ORIGINAL);


        for (Classifier cls : classifiers) {
            //buildAndTestClassifier(cls, dataset, reducedDataset, delta, datasetName);
            buildAndTestClassifier(
                    cls, train, test,
                    delta, datasetName);
        }

        MainForm.changeStatusIcon(true, statusIcon);

        return null;
    }

    private Classifier buildJ48() {

        J48 cls = new J48();
        cls.setConfidenceFactor((float) 0.25);
        cls.setMinNumObj(2);
        return cls;
    }

    private Classifier buildMultilayerPerceptron() {

        MultilayerPerceptron cls = new MultilayerPerceptron();

        cls.setLearningRate(0.3);
        cls.setMomentum(0.2);
        cls.setTrainingTime(500);
        cls.setValidationSetSize(0);
        cls.setSeed(0);
        cls.setValidationThreshold(20);
        cls.setHiddenLayers("0");

        return cls;
    }

    private Classifier buildNaiveBayes() {

        NaiveBayes cls = new NaiveBayes();

        return cls;
    }

    private void buildAndTestClassifier(Classifier classifier,
                                        Instances trainSet,
                                        Instances testSet,
                                        double delta,
                                        String datasetName) throws Exception {

        String filename, evalString;

        String classifierName = classifier.getClass().getName();

        classifierName = classifierName.substring(classifierName.lastIndexOf(".") + 1);

        logAndPublishToFrame("Starting building " + classifierName);
        classifier.buildClassifier(trainSet);
        logAndPublishToFrame("Finished building " + classifierName);

        logAndPublishToFrame("Saving model");
        filename = MessageFormat.format("./data/{0} -C {3} -P {1} -D {2}.model",
                datasetName, percentage, (new Double(delta)).toString(), classifierName);
        weka.core.SerializationHelper.write(filename, classifier);


        //train/test evaluation

        logAndPublishToFrame("Starting evaluation: train/test");
        Evaluation eval = new Evaluation(trainSet);
        eval.evaluateModel(classifier, testSet);
        logAndPublishToFrame("Finished evaluation: train/test");

        logAndPublishToFrame("Saving evaluation: train/test");
        filename = MessageFormat.format("./data/{0} -C {3} -P {1} -D {2} -T train-test.txt",
                datasetName, percentage, (new Double(delta)).toString(), classifierName);
        evalString = eval.toSummaryString("\n === Results ===\n", true);

        evalString += "\n" + eval.toClassDetailsString();

        evalString += "\n" + eval.toMatrixString();

        weka.core.SerializationHelper.write(filename, evalString);


        //k-fold evaluation

        logAndPublishToFrame("Starting evaluation: k-fold 10");
        eval = new Evaluation(trainSet);
        eval.crossValidateModel(classifier, trainSet, 10, new Random(1));
        logAndPublishToFrame("Finished evaluation: k-fold 10");

        logAndPublishToFrame("Saving evaluation: k-fold 10");
        filename = MessageFormat.format("./data/{0} -C {3} -P {1} -D {2} -T k-fold.txt",
                datasetName, percentage, (new Double(delta)).toString(), classifierName);
        evalString = eval.toSummaryString("\n === Results ===\n", true);

        evalString += "\n" + eval.toClassDetailsString();

        evalString += "\n" + eval.toMatrixString();

        weka.core.SerializationHelper.write(filename, evalString);


        //original eval
/*
        logAndPublishToFrame("Starting evaluation: original");
        eval = new Evaluation(trainSet);
        eval.evaluateModel(classifier, dataset);
        logAndPublishToFrame("Finished evaluation: original");

        logAndPublishToFrame("Saving evaluation: original");
        filename = MessageFormat.format("./data/{0} -C {3} -P {1} -D {2} -T original.txt",
                datasetName, percentage, (new Double(delta)).toString(), classifierName);
        evalString = eval.toSummaryString("\n === Results ===\n", true);

        evalString +=  "\n" + eval.toClassDetailsString();

        evalString +=  "\n" + eval.toMatrixString();

        weka.core.SerializationHelper.write(filename, evalString);

*/


        logAndPublishToFrame("Finished evaluation");
    }

    private void logAndPublishToFrame(String message) {
        logger.info(message);
        publish(message);
    }

    @Override
    protected void process(List<String> updates) {
        statusLabel.setText(updates.get(updates.size() - 1));
    }


}
