package worker;

import weka.core.Instance;
import weka.core.Instances;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by andre on 19/12/2016.
 */

public class DistributionCounter {


    class AttributeDistribution {
        int index;
        HashMap<Double, Double> hashMap = new HashMap<>();

        public AttributeDistribution(int index) {
            this.index = index;
        }

        public void addValueToCount(double value) {
            if (hashMap.containsKey(value)) {
                hashMap.put(value, hashMap.get(value) + 1);
            } else {
                hashMap.put(value, 1.0);
            }

        }
    }

    private AttributeDistribution[] attributeDistributions;

    public DistributionCounter(Instances dataset) {

        attributeDistributions = new AttributeDistribution[dataset.numAttributes()];

        for (int i = 0; i < dataset.numAttributes(); i++) {
            attributeDistributions[i] = new AttributeDistribution(i);
        }
        for (int i = 0; i < dataset.size(); i++) {
            Instance tempInstance = dataset.instance(i);
            for (int j = 0; j < dataset.numAttributes(); j++) {
                attributeDistributions[j].addValueToCount(tempInstance.value(j));
            }
        }

    }

    public DistributionCounter(int attributesNumber) {

        attributeDistributions = new AttributeDistribution[attributesNumber];
    }


    public DistributionCounter reduceDistributionTable(double percentage) {

        DistributionCounter dcCopy = new DistributionCounter(attributeDistributions.length);

        for (int i = 0; i < attributeDistributions.length; i++) {
            dcCopy.attributeDistributions[i] = new AttributeDistribution(attributeDistributions[i].index);
            Iterator it = attributeDistributions[i].hashMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                dcCopy.attributeDistributions[i].hashMap.put((Double) pair.getKey(), ((Double) pair.getValue() * percentage / 100));
                it.remove();
            }
        }
        return dcCopy;

    }


    public static double[] distributionDifference(DistributionCounter d1, DistributionCounter d2, Instances dataset) {

        double[] deltas = new double[d1.attributeDistributions.length];

        for (int i = 0; i < d1.attributeDistributions.length; i++) {
            int finalI = i;
            d1.attributeDistributions[i].hashMap.forEach((k, v) -> {
                double tempDiff = v;
                if (d2.attributeDistributions[finalI].hashMap.containsKey(k))
                    tempDiff = v - d2.attributeDistributions[finalI].hashMap.get(k);
                tempDiff = Math.abs(tempDiff);
                deltas[finalI] += tempDiff;

            });
            deltas[finalI] /= dataset.size();
        }

        return deltas;
    }
/*
    public static double weightedDeltaWithInstanceModification(DistributionCounter d1, DistributionCounter d2,
                                                               double[][] infoGain, double[] deltas,
                                                               Instance instance, Instances dataset, boolean adding) {
        double best = 0.0;
        double current = 0.0;

        double value;

        double[] updatedDelta;

        updatedDelta = Arrays.copyOf(deltas, deltas.length);

        for (int i = 0; i < instance.numAttributes(); i++) {

            value = instance.value(i);

            best = d1.attributeDistributions[i].hashMap.get(value);

            if (d2.attributeDistributions[i].hashMap.containsKey(value))
                current = d2.attributeDistributions[i].hashMap.get(value);
            else
                current = 0.0;

            updatedDelta[i] -= (Math.abs(best - current) / dataset.size());

            if (adding) {
                current += 1;
            } else {
                current -= 1;
            }

            updatedDelta[i] += (Math.abs(best - current) / dataset.size());
        }

        return weightedDelta(updatedDelta, infoGain);

    }
*/

    public static double[] weightedDeltaWithInstanceModification(DistributionCounter d1, DistributionCounter d2,
                                                                   double[][] infoGain, double[] deltas,
                                                                   Instance instance, Instances dataset, boolean adding) {
        double best = 0.0;
        double current = 0.0;

        double value;

        double[] updatedDelta;

        updatedDelta = Arrays.copyOf(deltas, deltas.length);

        for (int i = 0; i < instance.numAttributes(); i++) {

            value = instance.value(i);

            best = d1.attributeDistributions[i].hashMap.get(value);

            if (d2.attributeDistributions[i].hashMap.containsKey(value))
                current = d2.attributeDistributions[i].hashMap.get(value);
            else
                current = 0.0;

            updatedDelta[i] -= (Math.abs(best - current) / dataset.size());
            //updatedDelta[i] -= Math.abs(best - current);
            if (adding) {
                current += 1;
            } else {
                current -= 1;
            }

            updatedDelta[i] += (Math.abs(best - current) / dataset.size());

            //updatedDelta[i] += Math.abs(best - current);
        }

        return updatedDelta;

    }


    public static double weightedDelta(double[] deltas, double[][] infoGain) {
        double delta = 0;
        double totalInfoGain = 0.0;

        for (int i = 0; i < infoGain.length; i++) {
            totalInfoGain += infoGain[i][1];
        }
        for (int i = 0; i < infoGain.length; i++) {
            int index = (int) infoGain[i][0];
            delta += deltas[index] * infoGain[i][1] / totalInfoGain;
        }
        return delta;
    }


}