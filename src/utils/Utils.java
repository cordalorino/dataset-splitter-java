package utils;

import org.apache.log4j.Logger;
import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.core.Instances;

/**
 * Created by andreacorda on 13/12/16.
 */
public class Utils {

    public static Logger logger = Logger.getLogger(Utils.class);

    public static double[][] calculateInfoGain(Instances dataSet,double rankerThreshold){
        logger.debug("Starting InfoGainCalc");
        InfoGainAttributeEval eval = new InfoGainAttributeEval();
        AttributeSelection attsel = new AttributeSelection();
        Ranker search = new Ranker();
        search.setThreshold(rankerThreshold);
        logger.debug("Ranker Threshold = " + rankerThreshold);
        attsel.setEvaluator(eval);
        attsel.setSearch(search);
        double[][] attrRanks = new double[dataSet.numAttributes()][2];
        try {
            attsel.SelectAttributes(dataSet);
            attrRanks = attsel.rankedAttributes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.debug("Ranker results: \n" + attributeRanksToString(dataSet, attrRanks));
        return attrRanks;
    }

    private static String attributeRanksToString(Instances dataSet, double[][] attrRanks) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < attrRanks.length; i++){
            stringBuilder.append("\t");
            stringBuilder.append(dataSet.attribute((int)attrRanks[i][0]).name());
            stringBuilder.append("\t\t");
            stringBuilder.append(attrRanks[i][1]);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

}
